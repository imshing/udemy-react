import React from 'react'

const UserInput = (props) => {
  const inputStyle = {
    padding: '10px',
    borderRadius: '5px',
    border: '1px solid #ccc'
  }
  return (
    <div className="user-input">
      <input style={inputStyle} type="text" onChange={props.change} value={props.username}/>
    </div>
  )
}

export default UserInput
