import React from 'react'
import './UserOutput.css'

const UserOutput = (props) => {
  return (
    <div className="user-output">
      <p>Username: {props.username}</p>
      <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Doloremque quia quo perspiciatis facilis, labore quidem, laborum soluta, dolorem odit minus sequi eligendi asperiores nisi! Vero sit alias laboriosam excepturi asperiores?</p>
    </div>
  )
}

export default UserOutput
