import React, { Component } from 'react';
import classes from './App.css';
import Persons from '../components/Persons/Persons';
import Cockpit from '../components/Cockpit/Cockpit';
import withClass from '../hoc/withClass';
import Aux from '../hoc/Aux';
import AuthContext from '../context/auth-context'

class App extends Component {
  constructor(props) {
    super(props);
    console.log('[App.js] constructor')
  }
  state = {
    persons: [
      { id: 'asdf1', name: 'Max', age: 28 },
      { id: 'qwer2', name: 'Matthew', age: 33 },
      { id: 'zxcv3', name: 'Stephen', age: 99 }
    ],
    showPerson: false,
    changeCounter: 0,
    authenticated: false,
  }

  static getDerivedStateFromProps(props, state) {
    console.log('[App.js] getDerivedStateFromProps', props, state)
    return state
  }

  componentDidMount() {
    console.log('[App.js] componentDidMount()')
  }

  shouldComponentUpdate(nextProps, nextState) {
    console.log('[App.js] shouldComponentUpdate')
    return true;
  }

  componentDidUpdate() {
    console.log('[App.js] componentDidUpdate')
  }

  nameChangeHandler = (event, id) => {
    const personIndex = this.state.persons.findIndex(person => {
      return person.id === id;
    });

    const person = {
      ...this.state.persons[personIndex]
    }

    person.name = event.target.value;

    const persons = [...this.state.persons];
    persons[personIndex] = person;

    this.setState((prevState, props) => { 
      return {
        persons: persons,
        changeCounter: prevState.changeCounter + 1
      }      
    });
  }

  deletePersonHandler = (personIndex) => {
    // const persons = this.state.persons.slice();
    const persons = [...this.state.persons];
    persons.splice(personIndex, 1);
    this.setState({persons: persons})
  }

  togglePersonHandler = () => {
    const doesShow = this.state.showPerson;
    this.setState({showPerson: !doesShow});
  }

  loginHandler = () => {
    this.setState( { authenticated: true });
  }

  render() {
    console.log('[App.js] render()')
    let persons = null;
    if (this.state.showPerson) {
      persons = <Persons 
        persons={this.state.persons}
        click={this.deletePersonHandler}
        change={this.nameChangeHandler}
        isAuthenticated={this.state.authenticated}
      />
    }

    return (
      <Aux classes={classes.App}>
        <AuthContext.Provider value={{
          authenticated: this.state.authenticated,
          login: this.loginHandler
        }}>
          <Cockpit 
            showPerson={this.state.showPerson} 
            personsLength={this.state.persons.length} 
            clicked={this.togglePersonHandler} 
            title={this.props.appTitle}
            login={this.loginHandler}
          />
          {persons}
        </AuthContext.Provider>
      </Aux>      
    );
  }
}

export default withClass(App, classes.App);
