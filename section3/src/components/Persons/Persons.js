import React, { PureComponent } from 'react'
import Person from './Person/Person'

class Persons extends PureComponent {
  // static getDerivedStateFromProps(props, state) {
  //   console.log('[Persons.js] getDerivedStateFromProps')
  //   return state;
  // }

  shouldComponentUpdate(nextProps, nextState) {
    console.log('[Persons.js] shouldComponentUpdate')
    if (nextProps.persons !== this.props.persons || nextProps.change !== this.props.change || nextProps.click !== this.props.click) {
      return true
    } else {
      return false
    }
  }

  getSnapshotBeforeUpdate(prevProps, prevState) {
    console.log('[Persons.js] getSnapshoteBeforeUpdate');
    return {
      message: 'Snapshot!'
    }
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    console.log('[Persons.js] componentDidUpdate', snapshot)
  }

  componentWillUnmount() {
    console.log('[Persons.js] componentWillUnmount')
  }

  render() {
    console.log('[Persons.js] rendering...')
    return (   
      this.props.persons.map((person, index) => {
        return (
          <Person
            key={person.id}
            click={() => this.props.click(index)}
            name={person.name}
            age={person.age}                    
            change={(event) => this.props.change(event, person.id)}
            isAuth={this.props.isAuthenticated}
          />
        )
      })
    )
  }
}

export default Persons